@Library('maven-pom-parser')

def service = "aos"
def version = "maven-3.6.0"
def dockerRegistry = "registry.internal.hke.holdings"
def company = "/mmk"
def envs = ["dev", "uat", "master", "ext"]
def active_branch = false

pipeline {
    agent any

    tools {
        maven version
    }

    stages {
        stage ("Testing") {
            steps {
                //TODO: Run unit test later
                echo "Testing"
            }
        }

        stage ("Building and Deploying related service model") {
            steps {
                echo 'Building Service Model'
                script {
                    echo "service = " + service
                    if (env.CHANGE_AUTHOR == 'devops') {
                        buildDependencies("dev", service)
                    } else {
                        buildDependencies(env.BRANCH_NAME, service)
                    }
                }
            }
        }

        stage ("Building Service") {
            steps {
                echo 'Building Docker Image'
                script {
                    echo "I am building " + service + " in " + env.BRANCH_NAME + " branch"
                    for (theEnv in envs) {
                        if (env.BRANCH_NAME == theEnv) {
                            sh "mvn -P$theEnv -Dmaven.test.skip=true clean install -s /data/config/$theEnv-settings.xml"
                            active_branch = true
                            break
                        }
                    }
                    if (active_branch == false) {
                        sh "mvn -Pdev -Dmaven.test.skip=true clean install -s /data/config/dev-settings.xml"
                    }
                    sh 'git submodule update --init --recursive --remote'
                    sh "docker build . --no-cache -t " + dockerRegistry + company + "/" + service + ":" + env.BRANCH_NAME
                }
            }
        }

        stage ("Pushing to Docker Registry") {
            steps {
                script {
                    for (theEnv in envs) {
                        if (env.BRANCH_NAME == theEnv) {
                            withCredentials([usernamePassword(credentialsId: 'docker_pw', passwordVariable: 'docker_pw', usernameVariable: 'docker_user')]) {
                                sh 'docker login ' + dockerRegistry + ' -u $docker_user -p $docker_pw'
                            }
                        echo "I am pushing " + service + " to docker registry in " + env.BRANCH_NAME + " branch"
                        sh "docker push " + dockerRegistry + company + "/" + service + ":" + env.BRANCH_NAME
                        }
                    }
                }
            }
        }

        stage ("Deploying in K8s") {
            steps {
                sshagent(['dev-k8s']) {
                    script {
                        if (env.BRANCH_NAME == 'dev') {
                            try {
                                withCredentials([string(credentialsId: 'dev_k8s_pw', variable: 'dev_ssh_pw')]) {
                                    sh 'sshpass -p $dev_ssh_pw ssh root@172.28.36.181 -o StrictHostKeyChecking=no kubectl rollout restart deployment ' + service
                                }
                            } catch(error) {
                                withCredentials([string(credentialsId: 'dev_k8s_pw', variable: 'dev_ssh_pw')]) {
                                    sh 'sshpass -p $dev_ssh_pw ssh root@172.28.36.181 -o StrictHostKeyChecking=no "kubectl apply -f apps/' + service + '-app.yaml"'
                                }
                            }
                        } else if (env.BRANCH_NAME == 'ext') {
                            try {
                                withCredentials([string(credentialsId: 'ext_k8s_pw', variable: 'ext_ssh_pw')]) {
                                    sh 'sshpass -p $ext_ssh_pw ssh root@172.28.36.131 -o StrictHostKeyChecking=no kubectl rollout restart deployment ' + service
                                }
                            } catch(error) {
                                withCredentials([string(credentialsId: 'ext_k8s_pw', variable: 'ext_ssh_pw')]) {
                                    sh 'sshpass -p $ext_ssh_pw ssh root@172.28.36.131 -o StrictHostKeyChecking=no "kubectl apply -f apps/' + service + '-app.yaml"'
                                }
                            }
                        } else {
                            echo 'I am not in dev branch'
                        }
                    }
                }
            }
        }
    }

    post {
        success {
            script{
                if (env.BRANCH_NAME == 'ext') {
                    slackSend (channel: '#adamosoft', message: "${env.JOB_NAME} is successful.", color: '#000000')
                } else {
                    slackSend (message: "${env.JOB_NAME} is successful.", color: '#000000')
                }
            }
        }
        failure {
            script{
                if (env.BRANCH_NAME == 'ext') {
                    slackSend (channel: '#adamosoft', message: "@channel ${env.JOB_NAME} failure. (${env.BUILD_URL})", color: '#FF0000')
                } else {
                    slackSend (message: "@channel ${env.JOB_NAME} failure. (${env.BUILD_URL})", color: '#FF0000')
                }
            }
        }
        fixed {
            script{
                if (env.BRANCH_NAME == 'ext') {
                    slackSend (channel: '#adamosoft', message: "${env.JOB_NAME} back to success.", color: '#00FF00')
                } else {
                    slackSend (message: "${env.JOB_NAME} back to success.", color: '#00FF00')
                }
            }
        }
    }

}

 def buildDependencies(branch, service) {
    echo "Building branch: " + branch + " for service: " + service
    for (i in PomDependencyParser.readFile(service, branch, this)) {
        echo "build job: " + i
        build job: i
    }
 }
